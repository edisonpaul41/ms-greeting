export const CONFIG = {
    AUTH_URL: process.env.AUTH_URL || 'http://localhost:5000',
    PORT: process.env.PORT || 5001,
};
