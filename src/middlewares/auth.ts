import { NextFunction, Request, Response } from 'express';
import * as http from 'http';
import request from 'request';
import { CONFIG } from '../config';

export function VerifyToken(req: Request, res: Response, next: NextFunction) {
  const requiredHeader = 'X-Parse-REST-API-Key';

  if (!req.header(requiredHeader.toLocaleLowerCase())) {
    res.status(401).send('Missing Header');
    return;
  }
  request.post(CONFIG.AUTH_URL + '/auth/verify', {
    json: {
      key: req.header('X-Parse-REST-API-Key'),
    },
  }, (error: http.ClientRequest, response: http.IncomingMessage, body: string) => {
    if (error) {
      res.status(500).send(error);
      return;
    }

    if (response.statusCode !== 200) {
      res.status(401).send();
      return;
    }
    next();
  });
}
